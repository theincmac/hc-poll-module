var express = require('express');
var router = express.Router();

var pollController = require('../controllers/pollController')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('zenroom goes here');
});
router.get('/about', function(req, res, next) {
  res.send('this is about things.');
});

router.get('/new',pollController.create);
router.get('/get_poll',pollController.get_poll);

module.exports = router;
