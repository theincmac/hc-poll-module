const zenroom = require('zenroom').default

const keygen_contract = `rule check version 1.0.0
Scenario 'simple': Create the keypair
Given that I am known as 'Puria'
When I create the keypair
Then print my data`

zenroom.script(keygen_contract).zencode_exec();