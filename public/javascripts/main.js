var Vue = require('vue/dist/vue.js');

Vue.component('app', require('./components/app.vue'));

const app = new Vue({
  el: '#app'
})