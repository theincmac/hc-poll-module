var express = require('express');


const zenroom = require('zenroom').default;

const keygen_contract = `rule check version 1.0.0
Scenario 'simple': Create the keypair
Given that I am known as 'Admin'
When I create the keypair
Then print my data`;

const savedLines = []
const printFunction = (text) => { savedLines.push(text) }

zenroom.init({
	print: printFunction
})


exports.create = function(req, res){
	zenroom.script(keygen_contract).zencode_exec();
	res.send(savedLines.join(" "));
}
